from django.contrib import admin
from .models import Question, SurveyAssignment, SurveyResponse, Choice, Survey

admin.site.register(Survey)
admin.site.register(Question)
admin.site.register(Choice)
admin.site.register(SurveyAssignment)
admin.site.register(SurveyResponse)
